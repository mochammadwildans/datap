package com.example.belajarspring.dataP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataPApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataPApplication.class, args);
	}

}
